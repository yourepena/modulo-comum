import { Test, TestingModule } from '@nestjs/testing';
import { ComumService } from './comum.service';

describe('ComumService', () => {
  let service: ComumService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ComumService],
    }).compile();
    service = module.get<ComumService>(ComumService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
